import DashboardLayout from "@/views/Layout/DashboardLayout.vue";
import MainLayout from "@/views/Layout/MainLayout.vue";
import AuthLayout from "@/views/Pages/AuthLayout.vue";
// GeneralViews
import NotFound from "@/views/GeneralViews/NotFoundPage.vue";

// Pages
const Main = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Dashboard/Main.vue");

const Dash = () =>
  import(
    /* webpackChunkName: "dashboard" */ "@/views/Dashboard/Dashboard-T.vue"
  );

const MainAdmin = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Dashboard/MainAdmin.vue");

const Login = () =>
  import(/* webpackChunkName: "pages" */ "@/views/Pages/Login.vue");
const Home = () =>
  import(/* webpackChunkName: "pages" */ "@/views/Pages/Home.vue");
const Register = () =>
  import(/* webpackChunkName: "pages" */ "@/views/Pages/Register.vue");
const Lock = () =>
  import(/* webpackChunkName: "pages" */ "@/views/Pages/Lock.vue");

//myApp
const Offers = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Offers.vue");
const PastWinners = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/PastWinners.vue");

//myApp

const Categories = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Categories.vue");
//myApp
const MailShots = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/MailShots.vue");
const CharityAdd = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/CharityAdd.vue");
  const AddTermAndConditions = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/AddTermAndConditions.vue");
const CharityEdit = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/CharityEdit.vue");

const TermsAndCondition = () =>
  import(
    /* webpackChunkName: "dashboard" */ "@/views/Heaven/TermsAndCondition.vue"
  );

const Benefits = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Benefits.vue");

const BenefitsAdd = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/BenefitsAdd.vue");

const PastWinnersAdd = () =>
  import(
    /* webpackChunkName: "dashboard" */ "@/views/Heaven/PastWinnersAdd.vue"
  );

//myApp
const Bookings = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Bookings.vue");
//myApp
const MyProperties = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/MyProperties.vue");
const Properties = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Properties.vue");
const addProperty = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/AddProperty.vue");
const editProperty = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/editProperty.vue");
//myApp
const Charity = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Charity.vue");
//myApp
const payouts = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Payouts.vue");

const AboutKeyrings = () =>
  import(
    /* webpackChunkName: "dashboard" */ "@/views/Heaven/AboutKeyrings.vue"
  );

const OurStory = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/OurStory.vue");

const amc = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/AMC.vue");

const users = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Users.vue");
const psrequest = () =>
  import(
    /* webpackChunkName: "dashboard" */ "@/views/Heaven/PropertySellingRequest.vue"
  );
const orders = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Orders.vue");

const settings = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Settings.vue");

const Promocodes = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/PromoCodes.vue");

const aboutus = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/AboutUs.vue");

const productss = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Productss.vue");

const Sales = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Sales.vue");

  const Order = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Order.vue");

// const //Categories = () =>
//   import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Categories.vue");

const Reports = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Reports.vue");

const Messages = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Dashboard/Messages.vue");

const time = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Time.vue");
  const subscribers = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Subscribers.vue");
const Raffle = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/Raffle.vue");
const updatePropertyHeroBanner = () =>
  import(/* webpackChunkName: "dashboard" */ "@/views/Heaven/UpdatePropertyHeroBanner.vue");

let myMenu = {
  path: "/fusion",
  component: MainLayout,
  name: "fusion",
  redirect: "/fusion/dashboard",
  children: [
    {
      path: "dashboard",
      name: "fusiondashboard",
      component: Dash,
      
    },
    {
      path: "admin",
      name: "fusiondashboard",
      component: MainAdmin,
      
    },
    {
      path: "promocodes",
      name: "promocodes",
      component: Promocodes,
    },
    {
      path: "order",
      name: "order",
      component: Order,
      props: true,

    },
    {
      path: "subscribers",
      name: "subscribers",
      component: subscribers,
    },
    {
      path: "psrequest",
      name: "psrequest",
      component: psrequest,
    },
    {
      path: "pastWinners",
      name: "pastWinners",
      component: PastWinners,
    },
    {
      path: "aboutus",
      name: "aboutus",
      component: aboutus,
    },
    {
      path: "users",
      name: "users",
      component: users,
    },
    {
      path: "time",
      name: "time",
      component: time,
    },
    {
      path: "mailshots",
      name: "mailshots",
      component: MailShots,
    },
    {
      path: "termsandcondition",
      name: "termsandcondition",
      component: TermsAndCondition,
    },

    {
      path: "benefits",
      name: "benefits",
      component: Benefits,
    },

    {
      path: "benefitsadd",
      name: "benefitsadd",
      component: BenefitsAdd,
    },
    {
      path: "pastWinnersAdd",
      name: "pastWinnersAdd",
      component: PastWinnersAdd,
      props: true,
    },
    {
      path: "reports",
      name: "reports",
      component: Reports,
    },
    {
      path: "categories",
      name: "categories",
      component: Categories,
    },
    {
      path: "bookings",
      name: "bookings",
      component: Bookings,
    },
    {
      path: "myproperties",
      name: "myproperties",
      component: MyProperties,
    },
    {
      path: "properties",
      name: "properties",
      component: Properties,
    },
    {
      path: "addProperty",
      name: "addProperty",
      component: addProperty,
      props: true,
      
    },
    {
      path: "editProperty",
      name: "editProperty",
      component: editProperty,
    },

    {
      path: "ourStory",
      name: "ourStory",
      component: OurStory,
    },

    {
      path: "charityAdd",
      name: "charityAdd",
      component: CharityAdd,
    },
    {
      path: "charityEdit",
      name: "charityEdit",
      component: CharityEdit,
      props: true,
    },

    {
      path: "AddTermAndConditions",
      name: "AddTermAndConditions",
      component: AddTermAndConditions,
      props: true,
    },

    // CHARITY
    {
      path: "charity",
      name: "charity",
      component: Charity,
    },

    {
      path: "aboutKeyrings",
      name: "aboutKeyrings",
      component: AboutKeyrings,
    },

    {
      path: "payouts",
      name: "payouts",
      component: payouts,
    },
    {
      path: "amc",
      name: "amc",
      component: amc,
    },
    {
      path: "productss",
      name: "productss",
      component: productss,
    },
    {
      path: "sales",
      name: "Sales",
      component: Sales,
    },
    {
      path: "orders",
      name: "orders",
      component: orders,
      props: true,
    },
    {
      path: "raffle",
      name: "raffle",
      component: Raffle,
    },
    {
      path: "settings",
      name: "settings",
      component: settings,
    },
    {
      path: "offers",
      name: "offers",
      component: Offers,
    },
    {
      path: "messages",
      name: "messages",
      component: Messages,
    },
    {
      path: "UpdatePropertyHeroBanner",
      name: "UpdatePropertyHeroBanner",
      component: updatePropertyHeroBanner,
    }
  ],
};

let authPages = {
  path: "/",
  component: AuthLayout,
  name: "Authentication",
  children: [
    {
      path: "/home",
      name: "Home",
      component: Home,
      meta: {
        noBodyBackground: true,
      },
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
    },
    {
      path: "/register",
      name: "Register",
      component: Register,
    },
    {
      path: "/lock",
      name: "Lock",
      component: Lock,
    },
    { path: "*", name: "404", component: NotFound },
  ],
};

const routes = [
  {
    path: "/",
    redirect: "/login",
    name: "Home",
  },
  myMenu,
  authPages,
];

export default routes;
